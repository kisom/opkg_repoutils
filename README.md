## shut up and take my packages
This is a utility to build an `opkg` repository from a set of
packages and descriptions.

### Repo source layout
A repository should be structured like this:

    repo/               # can be whatever name, this is the top level directory
    |- source/          # contains package files and metadata
    |  |- eglibc/       # libc version
    |  |  |- armv7a/    # architecture
    |  |  |  |- package_name/
    |  |  |  |  |- package-version.ipk
    |  |  |  |  |- package_dbg_version.ipk
    |  |  |  |  |-  ...
    |  |  |  |  |-  package_name.conf
    |  |  |  |- ...
    |  |  |- ...
    |  |- feed          # the repository is built under this directory
    |  |- repo.conf     # repository configuration file
    |  |- repo.db       # database storing repo information
    
The `source` directory and `feed` directory names are configurable in the
`repo.conf` file.

### The repo.conf file

The repo.conf file currently only pays attention to two entries:

* `source` should specify the source directory for the repo
* `feed` shoul specify the output directory for the repo

### Writing package configuration files
Package configurations are written in YAML; the syntax is very simple and
easily readable. A sample configuration might look like:

```
package: tmux.conf
version: 1.7
feed: core
depends:
 - libevent (>= 2.0.5)
 - libncurses5 (>= 5.9)
section: admin
maintainer: Kyle Isom <coder+pkgsrv@kyleisom.net>
filename: tmux_1.7-r0_armv7a.ipk
dbg: tmux-dbg_1.8-r0_armv7a.ipkg
dev: tmux-dev_1.8-r0_armv7a.ipkg
doc: tmux-doc_1.8-r0_armv7a.ipkg
description: |
  Terminal multiplexer
  tmux is a terminal multiplexer: it enables a number of terminals (or windows),
  each running a separate program, to be created, accessed, and controlled from
  a single screen. tmux may be detached from a screen and continue running in
  the background, then later reattached.
```

(This is, at the time of writing, the version of the tmux.conf used to
generate the tmux entry in my [pkgsrv](http://pkg.brokenlcd.net/).

The core entries required by all packages are:
* package: the name of the package being generated (all the -doc, -dev, etc...
entries are automatically generated).
* version: the software's version
* feed: the feed name; for example, this tmux configuration will reuslt in
files being copied to `${REPO_SOURCE}/eglibc/armv7a/core/tmux*}`.
* section: a general classification for the package; generally, these follow
[Debian](http://www.debian.org/) conventions.
* filename: the basename of the package file (which should be in the same
directory as the configuration file).
* description: a short summary of what the package is. Each line should be
indented at least one space.

Optional entries include:
* depends: a list of dependencies for the package; these should either be
all on one line immediately following the keyword and separated by spaces
(i.e.

```
depends: libevent (>= 2.0.5) libncurses5 (>= 5.9)
```

or using the YAML list format, like in the example. In the list format,
the entries should be preceded by a '` - `', that is a space, dash, and
a space. (There are other allowed formats in the YAML spec, and if you
wish to use them, you are welcome to).
* dbg, dev, doc, and staticdev: these point to the debug-symbol, development
(i.e. C headers), documentation (i.e. man pages), and static library
packages.

### Conventions
* Packages and their configuration files are stored in a `libc/arch/group`
directory in the repo source directory. The group refers to a logical
grouping of packages, and not to the feed.
* Packages are output in a `libc/arch/feed/` directory in the repo feed
directory.

### Thanks

* [Aaron Bieber](http://qbit.io) let me bounce ideas off him. He is the
project's official GRAND RUBBER DUCKY. Also, he came up with the name.
